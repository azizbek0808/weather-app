package com.example.weatherapp.controller;

import com.example.weatherapp.criteria.CityCriteria;
import com.example.weatherapp.dto.ResponseWeather;
import com.example.weatherapp.service.IWeatherService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class WeatherController {
    private final IWeatherService weatherService;

    @GetMapping("/weather")
    public ResponseEntity<ResponseWeather> getLastAndNextWeather(@Valid CityCriteria criteria) {
        return ResponseEntity.ok().body(weatherService.getWeather(criteria.getName()));
    }

    @RequestMapping("/download/{cityName}")
    public void downloadFile(@PathVariable String cityName, HttpServletRequest request, HttpServletResponse response) {
        weatherService.getWeatherReport(cityName, request, response);
    }

}
