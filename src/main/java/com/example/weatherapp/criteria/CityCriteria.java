package com.example.weatherapp.criteria;

import lombok.Data;

@Data
public class CityCriteria {
    private String name;
}
