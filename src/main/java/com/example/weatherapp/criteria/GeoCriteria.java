package com.example.weatherapp.criteria;

import lombok.Data;

@Data
public class GeoCriteria {
    private String lat;
    private String lon;
}
