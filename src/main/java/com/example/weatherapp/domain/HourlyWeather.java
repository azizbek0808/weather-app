package com.example.weatherapp.domain;

import com.example.weatherapp.utils.LocalDateTimeConverter;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

@Entity
@Data
@Table(name = "hourly_weather")
public class HourlyWeather implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true)
    private Long id;

    @CreatedDate
    @Column(name = "created_date")
    private Date createdDate = new Date();

    @Column(name = "date_time")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime dateTime;

    @Column(name = "lat")
    private double lat;

    @Column(name = "lon")
    private double lon;

    @Column(name = "temp")
    private double temp;

    @Column(name = "feels_like")
    private double feels_like;

    @Column(name = "pressure")
    private int pressure;

    @Column(name = "humidity")
    private int humidity;

    @Column(name = "dew_point")
    private double dew_point;

    @Column(name = "uvi")
    private double uvi;

    @Column(name = "clouds")
    private int clouds;

    @Column(name = "visibility")
    private int visibility;

    @Column(name = "wind_speed")
    private double wind_speed;

    @Column(name = "wind_deg")
    private int wind_deg;

    @Column(name = "wind_gust")
    private double wind_gust;

    @Column(name = "weather_main")
    private String weather_main;

    @Column(name = "weather_description")
    private String weather_description;

    @Column(name = "pop")
    private double pop;

}
