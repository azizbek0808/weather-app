package com.example.weatherapp.dto;

import lombok.Data;

@Data
public class CoordDto {
    public double lon;
    public double lat;
}
