package com.example.weatherapp.dto;

import java.util.ArrayList;

public class CurrentDto {
    public int dt;
    public int sunrise;
    public int sunset;
    public double temp;
    public double feels_like;
    public int pressure;
    public int humidity;
    public double dew_point;
    public int uvi;
    public int clouds;
    public int visibility;
    public double wind_speed;
    public int wind_deg;
    public ArrayList<WeatherDto> weather;
}
