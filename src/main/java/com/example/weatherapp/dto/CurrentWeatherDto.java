package com.example.weatherapp.dto;

import lombok.Data;

import java.util.ArrayList;

@Data
public class CurrentWeatherDto {
    public CoordDto coord;
    public ArrayList<WeatherDto> weather;
    public String base;
    public MainDto main;
    public int visibility;
    public WindDto wind;
    public CloudsDto clouds;
    public int dt;
    public SysDto sys;
    public int timezone;
    public int id;
    public String name;
    public int cod;
}
