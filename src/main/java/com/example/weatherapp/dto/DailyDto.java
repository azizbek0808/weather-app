package com.example.weatherapp.dto;

import java.util.ArrayList;

public class DailyDto {
    public int dt;
    public int sunrise;
    public int sunset;
    public int moonrise;
    public int moonset;
    public double moon_phase;
    public TempDto temp;
    public FeelsLikeDto feels_like;
    public int pressure;
    public int humidity;
    public double dew_point;
    public double wind_speed;
    public int wind_deg;
    public double wind_gust;
    public ArrayList<WeatherDto> weather;
    public int clouds;
    public double pop;
    public double uvi;
    public double rain;
}
