package com.example.weatherapp.dto;

public class FeelsLikeDto {
    public double day;
    public double night;
    public double eve;
    public double morn;
}
