package com.example.weatherapp.dto;

import lombok.Data;

@Data
public class GeoDto {
    public String name;
    public String lat;
    public String lon;
    public String country;
    public String state;
}
