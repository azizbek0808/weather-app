package com.example.weatherapp.dto;

import lombok.Data;

import java.util.ArrayList;

@Data
public class HourlyWeatherDto {
    public double lat;
    public double lon;
    public String timezone;
    public int timezone_offset;
    public CurrentDto current;
    public ArrayList<HourlyDto> hourly;
    public ArrayList<DailyDto> daily;
}
