package com.example.weatherapp.dto;

import lombok.Data;

@Data
public class ResponseWeather {
    private CurrentWeatherDto current;
    private HourlyWeatherDto past;
    private HourlyWeatherDto next;
}
