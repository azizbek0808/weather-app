package com.example.weatherapp.dto;

public class SysDto {
    public int type;
    public int id;
    public String country;
    public int sunrise;
    public int sunset;
}
