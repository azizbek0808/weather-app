package com.example.weatherapp.dto;

public class TempDto {
    public double day;
    public double min;
    public double max;
    public double night;
    public double eve;
    public double morn;
}
