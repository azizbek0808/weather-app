package com.example.weatherapp.dto;

import lombok.Data;

@Data
public class WeatherDto {
    public int id;
    public String main;
    public String description;
    public String icon;
}
