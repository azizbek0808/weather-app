package com.example.weatherapp.repository;

import com.example.weatherapp.domain.HourlyWeather;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HourlyRepository extends JpaRepository<HourlyWeather, Long> {

}
