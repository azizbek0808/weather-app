package com.example.weatherapp.service;

import com.example.weatherapp.domain.HourlyWeather;
import com.example.weatherapp.dto.*;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface IWeatherService {

    GeoDto getGeoByCityName(String cityName);

    CurrentWeatherDto getCurrentWeather(String lat, String lon);

    HourlyWeatherDto getHourlyWeatherNext(String lat, String lon);

    HourlyWeatherDto getHourlyWeatherPast(String lat, String lon);

    ResponseWeather getWeather(String cityName);

    HourlyWeather toEntity(HourlyDto dto);

    void saveToDb(ResponseWeather weather);

    void getWeatherReport(String cityName, HttpServletRequest request, HttpServletResponse response);

}
