package com.example.weatherapp.service;

import com.example.weatherapp.domain.HourlyWeather;
import com.example.weatherapp.dto.*;
import com.example.weatherapp.repository.HourlyRepository;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;

import static org.apache.poi.ss.usermodel.VerticalAlignment.CENTER;

@Service("weatherService")
@RequiredArgsConstructor
public class WeatherService implements IWeatherService {

    @Value(value = "${accuweather.apikey}")
    private String apiKey;

    private final RestTemplate restTemplate;
    private final HourlyRepository repository;


    @Override
    public GeoDto getGeoByCityName(String cityName) {
        String url = "http://api.openweathermap.org/geo/1.0/direct?q=" + cityName + "&limit=1&appid=" + apiKey;
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<GeoDto[]> response = restTemplate.exchange(url, HttpMethod.GET, entity, GeoDto[].class);
        if (response.getStatusCode() == HttpStatus.OK && response.getBody() != null && response.getBody().length > 0) {
            return Arrays.stream(response.getBody()).findFirst().get();
        }
        return null;
    }

    @Override
    public CurrentWeatherDto getCurrentWeather(String lat, String lon) {
        CurrentWeatherDto currentWeatherDto = new CurrentWeatherDto();
        String url = "http://api.openweathermap.org/data/2.5/weather?lat=" + lat + "&lon=" + lon + "&appid=" + apiKey + "&units=metric";
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<CurrentWeatherDto> response = restTemplate.exchange(url, HttpMethod.GET, entity, CurrentWeatherDto.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            currentWeatherDto = response.getBody();
        }
        return currentWeatherDto;
    }

    @Override
    public HourlyWeatherDto getHourlyWeatherNext(String lat, String lon) {
        HourlyWeatherDto weather = null;
        String url = "https://api.openweathermap.org/data/2.5/onecall?lat=" + lat + "&lon=" + lon + "&exclude=minutely&appid=" + apiKey + "&units=metric";
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<HourlyWeatherDto> response = restTemplate.exchange(url, HttpMethod.GET, entity, HourlyWeatherDto.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            weather = response.getBody();
            if (weather != null && weather.getHourly() != null && weather.getHourly().size() > 0) {
                ArrayList<HourlyDto> hourly = weather.getHourly();
                weather.setHourly(new ArrayList<>(hourly.subList(1, 13)));
            }
        }
        return weather;
    }

    @Override
    public HourlyWeatherDto getHourlyWeatherPast(String lat, String lon) {
        HourlyWeatherDto weatherYesterday;
        HourlyWeatherDto weatherToday;
        HourlyWeatherDto responseDto = new HourlyWeatherDto();
        ArrayList<HourlyDto> list = new ArrayList<>();
        long epochTimeToday = OffsetDateTime.now(ZoneOffset.UTC).minusSeconds(20).toEpochSecond();
        long epochTimeYesterday = OffsetDateTime.now(ZoneOffset.UTC).minusDays(1).toEpochSecond();

        String url = "https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=" + lat + "&lon=" + lon + "&dt=" + epochTimeYesterday + "&appid=" + apiKey + "&units=metric";
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<HourlyWeatherDto> response = restTemplate.exchange(url, HttpMethod.GET, entity, HourlyWeatherDto.class);
        if (response.getStatusCode() == HttpStatus.OK) {
            weatherYesterday = response.getBody();
            if (weatherYesterday != null && weatherYesterday.getHourly() != null && weatherYesterday.getHourly().size() > 0) {
                list.addAll(weatherYesterday.getHourly());
            }
        }

        String url2 = "https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=" + lat + "&lon=" + lon + "&dt=" + epochTimeToday + "&appid=" + apiKey + "&units=metric";
        ResponseEntity<HourlyWeatherDto> response2 = restTemplate.exchange(url2, HttpMethod.GET, entity, HourlyWeatherDto.class);
        if (response2.getStatusCode() == HttpStatus.OK) {
            weatherToday = response2.getBody();
            if (weatherToday != null && weatherToday.getHourly() != null && weatherToday.getHourly().size() > 0) {
                list.addAll(weatherToday.getHourly());
            }
        }

        if (list.size() > 12) {
            responseDto.setHourly(new ArrayList<>(list.subList(list.size() - 12, list.size())));
        }

        return responseDto;
    }

    @Override
    public ResponseWeather getWeather(String cityName) {
        ResponseWeather weather = new ResponseWeather();
        GeoDto geo = getGeoByCityName(cityName);
        if (geo != null) {
            weather.setCurrent(getCurrentWeather(geo.getLat(), geo.getLon()));
            weather.setPast(getHourlyWeatherPast(geo.getLat(), geo.getLon()));
            weather.setNext(getHourlyWeatherNext(geo.getLat(), geo.getLon()));
        }
        saveToDb(weather);
        return weather;
    }

    @Override
    public HourlyWeather toEntity(HourlyDto dto) {
        if (dto == null) {
            return null;
        }
        WeatherDto weather = dto.getWeather().get(0);
        HourlyWeather entity = new HourlyWeather();
        entity.setDateTime(LocalDateTime.ofInstant(Instant.ofEpochSecond(dto.getDt()), TimeZone.getTimeZone("GMT+5").toZoneId()));
        entity.setTemp(dto.getTemp());
        entity.setFeels_like(dto.getFeels_like());
        entity.setPressure(dto.getPressure());
        entity.setHumidity(dto.getHumidity());
        entity.setDew_point(dto.getDew_point());
        entity.setUvi(dto.getUvi());
        entity.setClouds(dto.getClouds());
        entity.setVisibility(dto.getVisibility());
        entity.setWind_speed(dto.getWind_speed());
        entity.setWind_deg(dto.getWind_deg());
        entity.setWind_gust(dto.getWind_gust());
        entity.setWeather_main(weather.getMain());
        entity.setWeather_description(weather.getDescription());
        entity.setPop(dto.getPop());
        return entity;
    }

    @Override
    public void saveToDb(ResponseWeather weather) {
        double lat = weather.getCurrent().getCoord().getLat();
        double lon = weather.getCurrent().getCoord().getLon();
        List<HourlyDto> list = new ArrayList<>();
        list.addAll(weather.getPast().hourly);
        list.addAll(weather.getNext().hourly);
        for (HourlyDto dto : list) {
            HourlyWeather entity = toEntity(dto);
            entity.setLat(lat);
            entity.setLon(lon);
            repository.save(entity);
        }
    }

    @Override
    public void getWeatherReport(String cityName, HttpServletRequest request, HttpServletResponse response) {
        int v = 0;
        ResponseWeather weather = getWeather(cityName);
        if (weather != null) {
            List<HourlyDto> list = new ArrayList<>();
            list.addAll(weather.getPast().hourly);
            list.addAll(weather.getNext().hourly);

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
            String title = weather.getCurrent().name + " bo'yicha 12 soat oldinga va keyingi ob-havo ma'lumotlari";
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.createSheet(title);
            sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 15));

            XSSFRow rowHead = sheet.createRow((short) 0);
            rowHead.setHeightInPoints(30);

            XSSFCell cellHead = rowHead.createCell(0);
            cellHead.setCellValue(title);

            XSSFFont font = workbook.createFont();
            font.setColor((short) 10);
            font.setFontHeightInPoints((short) 22);
            font.setColor(IndexedColors.WHITE.getIndex());

            XSSFCellStyle xssfCellStyle = workbook.createCellStyle();
            xssfCellStyle.setFont(font);

            rowHead.setRowStyle(xssfCellStyle);

            CellStyle styleHead = workbook.createCellStyle();
            styleHead.setBorderBottom(BorderStyle.THIN);
            styleHead.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
            styleHead.setFillBackgroundColor(IndexedColors.AQUA.getIndex());
            styleHead.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleHead.setAlignment(HorizontalAlignment.CENTER);
            styleHead.setVerticalAlignment(CENTER);
            styleHead.setFont(font);
            cellHead.setCellStyle(styleHead);
            // header end

            XSSFFont font1 = workbook.createFont();
            font1.setFontHeightInPoints((short) 11);
            font1.setBold(true);
            font1.setColor(IndexedColors.WHITE.getIndex());

            XSSFRow rowHeader = sheet.createRow((short) 1);
            rowHeader.setHeightInPoints(40);

            CellStyle styleCol = workbook.createCellStyle();
            styleCol.setAlignment(HorizontalAlignment.CENTER);
            styleCol.setVerticalAlignment(CENTER);
            XSSFFont fontCol = workbook.createFont();
            styleCol.setFont(fontCol);

            CellStyle styleHeader = workbook.createCellStyle();
            styleHeader.setBorderBottom(BorderStyle.THIN);
            styleHeader.setBorderTop(BorderStyle.THIN);
            styleHeader.setBorderLeft(BorderStyle.THIN);
            styleHeader.setBorderRight(BorderStyle.THIN);
            styleHeader.setWrapText(true);
            styleHeader.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
            styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
            styleHeader.setAlignment(HorizontalAlignment.CENTER);
            styleHeader.setVerticalAlignment(CENTER);
            styleHeader.setFont(font1);

            CellStyle styleCenter = workbook.createCellStyle();
            styleCenter.setAlignment(HorizontalAlignment.CENTER);
            styleCenter.setBorderBottom(BorderStyle.THIN);
            styleCenter.setBorderTop(BorderStyle.THIN);
            styleCenter.setBorderLeft(BorderStyle.THIN);
            styleCenter.setBorderRight(BorderStyle.THIN);
            styleCenter.setVerticalAlignment(CENTER);
            styleCenter.setWrapText(true);

            CellStyle styleRight = workbook.createCellStyle();
            styleRight.setAlignment(HorizontalAlignment.RIGHT);
            styleRight.setBorderBottom(BorderStyle.THIN);
            styleRight.setBorderTop(BorderStyle.THIN);
            styleRight.setBorderLeft(BorderStyle.THIN);
            styleRight.setBorderRight(BorderStyle.THIN);
            styleRight.setVerticalAlignment(CENTER);
            styleRight.setWrapText(true);

            CellStyle styleLeft = workbook.createCellStyle();
            styleLeft.setAlignment(HorizontalAlignment.LEFT);
            styleLeft.setBorderBottom(BorderStyle.THIN);
            styleLeft.setBorderTop(BorderStyle.THIN);
            styleLeft.setBorderLeft(BorderStyle.THIN);
            styleLeft.setBorderRight(BorderStyle.THIN);
            styleLeft.setVerticalAlignment(CENTER);
            styleLeft.setWrapText(true);

            Cell cell = rowHeader.createCell(0);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("№");
            cell = rowHeader.createCell(1);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Date time");
            cell = rowHeader.createCell(2);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Temp");
            cell = rowHeader.createCell(3);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Feels like");
            cell = rowHeader.createCell(4);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Pressure");
            cell = rowHeader.createCell(5);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Humidity");
            cell = rowHeader.createCell(6);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Dew point");
            cell = rowHeader.createCell(7);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Uvi");
            cell = rowHeader.createCell(8);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Clouds");
            cell = rowHeader.createCell(9);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Visibility");
            cell = rowHeader.createCell(10);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Wind speed");
            cell = rowHeader.createCell(11);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Wind deg");
            cell = rowHeader.createCell(12);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Wind gust");
            cell = rowHeader.createCell(13);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Weather main");
            cell = rowHeader.createCell(14);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Weather description");
            cell = rowHeader.createCell(15);
            cell.setCellStyle(styleHeader);
            cell.setCellValue("Weather pop");

            sheet.setColumnWidth(0, 7 * 256);
            sheet.setColumnWidth(1, 25 * 256);
            sheet.setColumnWidth(2, 12 * 256);
            sheet.setColumnWidth(3, 25 * 256);
            sheet.setColumnWidth(4, 25 * 256);
            sheet.setColumnWidth(5, 15 * 256);
            sheet.setColumnWidth(6, 25 * 256);
            sheet.setColumnWidth(7, 20 * 256);
            sheet.setColumnWidth(8, 20 * 256);
            sheet.setColumnWidth(9, 20 * 256);
            sheet.setColumnWidth(10, 20 * 256);
            sheet.setColumnWidth(11, 30 * 256);
            sheet.setColumnWidth(12, 30 * 256);
            sheet.setColumnWidth(13, 30 * 256);
            sheet.setColumnWidth(14, 30 * 256);
            sheet.setColumnWidth(15, 30 * 256);

            for (HourlyDto hourlyDto : list) {
                XSSFRow row = sheet.createRow((short) v + 2);
                cell = row.createCell(0);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(v + 1);

                cell = row.createCell(1);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(LocalDateTime.ofInstant(Instant.ofEpochSecond(hourlyDto.getDt()), TimeZone.getTimeZone("GMT+5").toZoneId()).format(formatter));

                cell = row.createCell(2);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getTemp());

                cell = row.createCell(3);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getFeels_like());

                cell = row.createCell(4);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getPressure());

                cell = row.createCell(5);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getHumidity());

                cell = row.createCell(6);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getDew_point());

                cell = row.createCell(7);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getUvi());

                cell = row.createCell(8);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getClouds());

                cell = row.createCell(9);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getVisibility());

                cell = row.createCell(10);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getWind_speed());

                cell = row.createCell(11);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getWind_deg());

                cell = row.createCell(12);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getWind_gust());

                cell = row.createCell(13);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getWeather().get(0).getMain());

                cell = row.createCell(14);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getWeather().get(0).getDescription());

                cell = row.createCell(15);
                cell.setCellStyle(styleCenter);
                cell.setCellValue(hourlyDto.getPop());

                v++;
            }
            try {
                File file = new File(weather.getCurrent().name + " - weather report.xlsx");
                OutputStream outputStream = new FileOutputStream(file);
                workbook.write(outputStream);
                outputStream.close();
                workbook.close();

                InputStream in = new FileInputStream(file);

                // Gets MIME type of the file
                String mimeType = new MimetypesFileTypeMap().getContentType(file.getName());

                if (mimeType == null) {
                    // Set to binary type if MIME mapping not found
                    mimeType = "application/octet-stream";
                }
                System.out.println("MIME type: " + mimeType);

                // Modifies response
                response.setContentType(mimeType);
                response.setContentLength((int) file.length());

                // Forces download
                String headerKey = "Content-Disposition";
                String headerValue = String.format("attachment; filename=\"%s\"", file.getName());
                response.setHeader(headerKey, headerValue);

                // obtains response's output stream
                OutputStream outStream = response.getOutputStream();

                byte[] buffer = new byte[4096];
                int bytesRead = -1;

                while ((bytesRead = in.read(buffer)) != -1) {
                    outStream.write(buffer, 0, bytesRead);
                }

                in.close();
                outStream.close();

                if (file.delete()) {
                    System.out.println("File deleted successfully");
                } else {
                    System.out.println("Failed to delete the file");
                }

                System.out.println("File downloaded at client successfully");

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }


}
