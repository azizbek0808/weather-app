<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Weather</title>
    <link rel="icon" href="../../resources/images/icon.png"/>
    <link rel="stylesheet" href="../../resources/css/style.css"/>

    <style>
        .row {
            display: grid;
            grid-template-columns: 1fr 1fr;
        }
    </style>
</head>
<body style="filter: blur(0)">
<div class="Weather">
    <input
            type="search"
            list="suggestions"
            class="weather__search"
            placeholder="Please write any city"/>
    <datalist id="suggestions">
        <option value="Tashkent"></option>
        <option value="Namangan"></option>
        <option value="Paris"></option>
        <option value="New York"></option>
        <option value="London"></option>
        <option value="New Orleans"></option>
    </datalist>

    <button style="width: 5%; height: 40px!important; background-color: #009cdc; color: white; border-radius: 5px" onclick="exportData()" >Export</button>

    <div class="border border-primary m-3">
        <div class="row">
            <div class="col-6" id="past-chart">

            </div>
            <div class="col-6" id="next-chart">

            </div>
        </div>
    </div>
</div>

<script src="../../resources/js/apexcharts.js"></script>

<script src="../../resources/js/weather.js"></script>
</body>
</html>

