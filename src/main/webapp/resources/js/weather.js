let searchInp = document.querySelector(".weather__search");
let day = document.querySelector(".weather__day");
let image = document.querySelector(".weather__image");

//  set city weather info
searchInp.addEventListener("keydown", async (e) => {
    if (e.keyCode === 13) {
        getWeater(searchInp.value);
    }
});

const getWeater = (city) => {
    fetch("/weather?name=" + city)
        .then((response) => response.json())
        .then((response) => {
            console.log(response);
            datesPast = response.past.hourly.map((list) => {
                return new Date(list.dt * 1000).toLocaleString();
            });
            tempsPast = response.past.hourly.map((list) => {
                return list.temp;
            });

            datesNext = response.next.hourly.map((list) => {
                return new Date(list.dt * 1000).toLocaleString();
            });

            tempsNext = response.next.hourly.map((list) => {
                return list.temp;
            });
            let pastOptions = {
                series: [
                    {
                        name: "Temp",
                        data: tempsPast,
                    },
                ],
                chart: {
                    height: 400,
                    type: "area",
                },
                dataLabels: {
                    enabled: true,
                },
                stroke: {
                    curve: "smooth",
                },

                xaxis: {
                    type: "time",
                    categories: datesPast,
                    labels: {
                        rotate: -70,
                        maxHeight: 140,
                    },
                    time: {
                        parser: "MM/DD HH:mm",
                        tooltipFormat: "HH:mm",
                        unit: "hour",
                        unitStepSize: 3,
                        displayFormats: {
                            hour: "MM/DD hA",
                        },
                    },
                },
            };
            let nextOptions = {
                series: [
                    {
                        name: "Temp",
                        data: tempsNext,
                    },
                ],
                chart: {
                    height: 400,
                    type: "area",
                },
                dataLabels: {
                    enabled: true,
                },
                stroke: {
                    curve: "smooth",
                },

                xaxis: {
                    type: "time",
                    categories: datesNext,
                    labels: {
                        rotate: -70,
                        maxHeight: 140,
                    },
                    time: {
                        parser: "MM/DD HH:mm",
                        tooltipFormat: "HH:mm",
                        unit: "hour",
                        unitStepSize: 3,
                        displayFormats: {
                            hour: "MM/DD hA",
                        },
                    },
                },
            };
            let pastChart = new ApexCharts(
                document.querySelector("#past-chart"),
                pastOptions
            );
            pastChart.render();

            let nextChart = new ApexCharts(
                document.querySelector("#next-chart"),
                nextOptions
            );
            nextChart.render();
        });
};

function exportData(){
    window.location.href = '/download/' + searchInp.value;
}
